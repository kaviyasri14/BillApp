import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import { Link } from "react-router-dom";
import Button from "react-bootstrap/Button";
import firebase from "./config/FirebaseConfig";
import Spinner from "react-bootstrap/Spinner";
import { Col} from "react-bootstrap";
import "./register.css"

function Register(props) {
  const [validated, setValidated] = useState(false);
  const [userName, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [creating, setCreating] = useState(false);
  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    setValidated(true);
  };
  return (
    <div className="registerPage">
    <div className="registerContainer">
    <h1>
         <Link to="/">
          <i className="fas fa-home home"></i>
         </Link>
           Register
         </h1>
      <Form noValidate validated={validated} onSubmit={handleSubmit}>
      <Form.Row>
        <Col  className = "textbox">        
              <Form.Control
              className = "username"
                required
                type="text"
                placeholder="UserName"
                value={userName}
                onChange={(e) => {
                  setUserName(e.target.value);
                }}
              />
        </Col>
      <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
      </Form.Row>
      <Form.Row>
        <Col  className = "textbox"> 
          <Form.Control
                  required
                  className = "email"
                  type="email"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                />
        </Col>
        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
      </Form.Row>
      <Form.Row>
        <Col  className = "textbox">
          <Form.Control
                required
                className = "password"
                minLength="6"
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
              />
        </Col>
        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
      </Form.Row>      
       <div className="submitButton">
       <Col > 
          <Button
          type="submit"
          // variant="outline-success"
          variant="success"
          disabled={creating}
          className="submitButton"
          onClick={(e) => {
            signUp(e);
          }}
        >
          {creating ? (
            <Spinner
              as="span"
              animation="border"
              size="sm"
              role="status"
              aria-hidden="true"
            />
          ) : (
            ""
          )}
          {creating ? "Creating.." : "Create Account"}
        </Button>
        </Col>
        </div>
      </Form>
      </div>
    </div>
  );
  async function signUp(e) {
    e.preventDefault();
    setCreating(true);
    let o = {
      uname: userName,
      role: "user",

    };
    try {
      await firebase.register(userName, email, password);
      await firebase.addUser(o);
      props.history.push("/login");
      setCreating(false);
    } catch (error) {
      alert(error.message);
      setCreating(false);
    }
  }
}
export default Register;
