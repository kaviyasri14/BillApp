import React, { Component } from 'react'
import moment from 'moment';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Col,Form,Button,Accordion,Card,Table,Container,Row } from 'react-bootstrap';
import firebase from "./config/FirebaseConfig";
import "./report.css"
import Select from 'react-select';

export default class Report extends Component {
    constructor(props) {
        super(props)
        this.state = {
            seletedArea: 3, areas: [], shops: [], endDate: new Date(),
            startDate: new Date(), dataLoaded: false, formatedBill: [],
            reportType: '',
            filterShop: '',
            totalAmount: 0
        }
        this.updateShops = this.updateShops.bind(this)
        this.fetchResults = this.fetchResults.bind(this)
    }
    componentDidMount() {
        firebase.getAllShops(this.updateShops)
    }
    updateShops(data) {
        let shopobj = {}
        data.forEach((t) => {
            if (Object.keys(t).length) {
                shopobj[Object.keys(t)[0]] = Object.values(t)[0]
            }
        })
        this.setState({
            areas: this.props.location.state,
            shops: shopobj
        }, () => { this.setState({ dataLoaded: true }) })
    }
    fetchResults(type) {
        this.setState({ formatedBill: [], reportType: type }, () => {
            let regex = new RegExp('^' + this.state.selectedArea + '_')
            let bills = []
            let resultbill = []
            for (let key in this.state.shops) {
                let obj = {}
                bills = []
                if (regex.test(key)) {
                    if (type === "debit" && this.state.shops[key].hasOwnProperty('debitDetails')) {
                        bills = this.state.shops[key].debitDetails.filter((s) => {
                            return moment(moment(s.debitDate).format("YYYY-MM-DD")).isAfter(moment(this.state.startDate).format("YYYY-MM-DD")) && moment(moment(s.debitDate).format("YYYY-MM-DD")).isBefore(moment(this.state.endDate).format("YYYY-MM-DD"))
                                || (moment(moment(s.debitDate).format("YYYY-MM-DD")).isSame(moment(this.state.endDate).format("YYYY-MM-DD")) || moment(moment(s.debitDate).format("YYYY-MM-DD")).isSame(moment(this.state.startDate).format("YYYY-MM-DD")));
                        })
                    } else if (type == 'add') {
                        bills = this.state.shops[key].bills.filter((s) => {
                            return moment(moment(s.modifiedDate).format("YYYY-MM-DD")).isAfter(moment(this.state.startDate).format("YYYY-MM-DD")) && moment(moment(s.modifiedDate).format("YYYY-MM-DD")).isBefore(moment(this.state.endDate).format("YYYY-MM-DD"))
                                || (moment(moment(s.modifiedDate).format("YYYY-MM-DD")).isSame(moment(this.state.endDate).format("YYYY-MM-DD"))
                                    || moment(moment(s.modifiedDate).format("YYYY-MM-DD")).isSame(moment(this.state.startDate).format("YYYY-MM-DD")));
                        })
                    }
                    if (bills.length) {
                        obj.shopName = this.state.shops[key].shopName
                        obj.bills = bills
                        obj.balance = this.state.shops[key].total
                    }
                    if (Object.keys(obj).length) {
                        resultbill.push(obj)
                    }
                }
                let total = 0
                resultbill.map((r)=>{r.bills.map(bill => {total+=Number(bill.amount)})})
                console.log(resultbill)
                this.setState({ formatedBill: resultbill,totalAmount: total })
            }
        })
    }


    render() {
        return (
            <div className="reportContainer">
                {/* <div > */}
                <Col lg={2}>
                        <Button
                    className="buttons"
                    variant="primary"
                    size="md"
                    onClick={() => {
                    this.props.history.push("/dashboard");
                    }}
                ><i className="fas fa-arrow-circle-left"></i>
                Back to Dashboard</Button>
                </Col>
                <Container className="datepickerContainer">

                    <Row>

                        <Col lg={2} className="center" > <label className="labels">Start Date</label></Col>
                        <Col lg={4} className="center"><DatePicker placeholderText="Start Date"
                            selected={this.state.startDate} onChange={(date) => {
                                this.setState({
                                    startDate: date,
                                    endDate: date
                                })
                            }} /></Col>
                        <Col lg={2} className="center" ><label className="labels">End Date</label></Col>
                        <Col lg={4} className="center" ><DatePicker
                            placeholderText="End Date"
                            selected={this.state.endDate}
                            onChange={(date) => {
                                this.setState({
                                    endDate: date
                                })
                            }}
                            minDate={this.state.startDate}
                            showDisabledMonthNavigation /></Col>
                    </Row>
                </Container>
                {/* </div> */}
                <Form.Group className="formContainer" >
                    <h2>Select line to Generate Bill</h2>
                    <Select
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={this.state.seletedArea}
                                isDisabled={false}
                                isLoading={false}
                                isClearable={true}
                                isRtl={false}
                                isSearchable={true}
                                name="linedrpdwn"
                                options={this.state.areas}
                                getOptionLabel={option => `${option.areaName}`}
                                getOptionValue={option => `${option.id}`}
                                onChange={option => {this.setState({
                                    selectedArea: option.id
                                })}}
                            />
                    {/* <Form.Control as="select" className="linedrpdwn" size="lg" defaultValue={this.state.seletedArea} onChange={(e) => {
                        this.setState({
                            selectedArea: e.target.value
                        })
                    }} >
                        {
                            this.state.areas.map(a => {
                                return (
                                    <option className="options" key={a.id} value={a.id}
                                    >{a.areaName}</option>
                                )
                            })
                        }
                    </Form.Control> */}
                    <div className="buttongrp">
                        <Button
                            className="buttons"
                            variant="success"
                            onClick={() => {
                                this.fetchResults("add")
                            }}
                        >
                            Add Bill Report
                        </Button>
                        <Button
                            className="buttons"
                            variant="danger"
                            onClick={() => {
                                this.fetchResults("debit")
                            }}
                        >
                            Debit Bill Report

                             </Button>
                    </div>
                </Form.Group>
                
                {
                    this.state.formatedBill.length? (
                        <>
                        <h4 className="totalamount">Total Amount: {this.state.totalAmount}</h4>
                <Form.Control type="text" placeholder="Filter by shopname" value={this.state.filterShop} onChange={(e) => { this.setState({ filterShop: e.target.value })}} />
                </>
                ) : ('')} 
                <Accordion defaultkey={0+''}>
                    {this.state.formatedBill.filter(f => f.shopName.toLowerCase().includes(this.state.filterShop.toLowerCase()) || this.state.filterShop === '').map((s, idx) => {
                        return (
                            <Card key={idx}>
                                <Card.Header>
                                    <Accordion.Toggle as={Button} className="header" variant="link" eventKey={idx+''}>
                                        <h5>{s.shopName} -(Total:{s.bills.reduce((a, b) => a + (Number(b['amount']) || 0), 0)})</h5>
                                        {/* <h6></h6> */}
                                    </Accordion.Toggle>
                                </Card.Header>
                                <Accordion.Collapse eventKey={idx+''}>
                                    <Card.Body>
                                        <Table striped bordered hover>
                                            <thead>
                                                <tr>
                                                    <th>S.No.</th>
                                                    <th>{this.state.reportType === 'debit' ? 'Debit' : 'Included'} Amount</th>
                                                    <th>{this.state.reportType === 'debit' ? 'Debit' : 'Created'} Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {s.bills.map((x, id) => {
                                                    return (
                                                        <tr key={id}>
                                                            <td>{id + 1}</td>
                                                            <td>{this.state.reportType === 'debit' ? x.amount : x.billAmount}</td>
                                                            <td>{this.state.reportType === 'debit' ? x.debitDate : x.modifiedDate}</td>
                                                        </tr>
                                                    )
                                                })}

                                            </tbody>
                                        </Table>
                                    </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                        )
                    })}
                </Accordion>

            </div>
        )
    }
}
