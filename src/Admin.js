import React, { Component } from 'react';
import {Button,Form,Col} from "react-bootstrap";
import Modal from 'react-bootstrap/Modal'
import firebase from "./config/FirebaseConfig";
import Select from 'react-select';

class Admin extends Component {
    constructor(props){
        super(props);
        this.state = {
            addArea: false,
            area: '',
            addShop: false,
            selectedArea: '',
            shopName: '',
            shops: []
        }
        this.handleOK = this.handleOK.bind(this)
        this.setShopCount = this.setShopCount.bind(this)
    }
    componentDidMount() {
        
    }
    setShopCount(data){
        this.setState({ shops: data.shops })
    }
    async handleOK(){
        let obj = {
            areaName: this.state.area,
            id: this.props.location.state.length + 1
        }
        let excistingArea = this.props.location.state
        excistingArea.push(obj)
        await firebase.addArea(excistingArea, obj.id);
        this.setState({'addArea':false})
    }
    async addShopToArea() {
        let obj = {
            id: this.state.selectedArea + '_' + (this.state.shops.length + 1),
            shopName: this.state.shopName
        }
        let shoparray = this.state.shops
        shoparray.push(obj)
        await firebase.putShopDetails(this.state.selectedArea,shoparray)
        this.setState({
            addShop:false
        })
    }
    render() {
        return (
            <div>
                  <Col lg={2}>
                        <Button
                    className="buttons"
                    variant="primary"
                    size="md"
                    onClick={() => {
                    this.props.history.push("/dashboard");
                    }}
                ><i className="fas fa-arrow-circle-left"></i>
                Back to Dashboard</Button>
                </Col>
                <Col>
                <Button
                    className="buttons"
                    variant="primary"
                    onClick={()=>{
                        this.setState({addArea: true})
                    }}
                >
                    Create Area
                </Button>
                <Button
                    className="buttons"
                    variant="danger"
                    onClick={()=>{
                        this.setState({addShop: true})
                    }}
                >
                    Map New Shop
                </Button>
                </Col>
                <Modal show={this.state.addArea} onHide={()=>{this.setState({'addArea':false})}}>
                    <Modal.Header closeButton>
                    <Modal.Title>Add Collection Area</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Control type="text" placeholder="Area" value={this.state.area} onChange={(e) => { this.setState({ area: e.target.value }) }} /><br />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                    <Button variant="secondary" onClick={()=>{this.setState({'addArea':false})}}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={()=>{this.handleOK()}}>
                        Save Changes
                    </Button>
                    </Modal.Footer>
                </Modal>
                <Modal show={this.state.addShop} onHide={()=>{this.setState({'addShop':false})}}>
                    <Modal.Header closeButton>
                    <Modal.Title>Add Shop to Area</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Control type="text" placeholder="Shop Name" value={this.state.shopName} onChange={(e) => { this.setState({ shopName: e.target.value }) }} /><br />
                            <Select
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={this.props.location.state[0].id}
                                isDisabled={false}
                                isLoading={false}
                                isClearable={true}
                                isRtl={false}
                                isSearchable={true}
                                name="linedrpdwn"
                                options={this.props.location.state}
                                getOptionLabel={option => `${option.areaName}`}
                                getOptionValue={option => `${option.id}`}
                                onChange={option => {
                                    this.setState({selectedArea: option.id},()=>{firebase.getShopDetails(this.setShopCount, this.state.selectedArea)})
                                    
                                }}
                            />
                            {/* <Form.Control as="select" size="lg" defaultValue={this.state.selectedArea} onChange={(e)=>{
                                this.setState({selectedArea: e.target.value},()=>{firebase.getShopDetails(this.setShopCount, this.state.selectedArea)})
                                
                            }}>
                        {
                            this.props.location.state.map(a => {
                                return (
                                    <option key={a.id} value={a.id}
                                    >{a.areaName}</option>
                                )
                            })
                        }
                    </Form.Control> */}
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                    <Button variant="secondary" onClick={()=>{this.setState({'addShop':false})}}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={()=>{this.addShopToArea()}}>
                        Add Shop
                    </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default Admin;
