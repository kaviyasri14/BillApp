import React, { Component } from 'react'
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Modal from 'react-bootstrap/Modal'
import firebase from "./config/FirebaseConfig";
import { Col } from "react-bootstrap";
import "./dashboard.css"
import moment from 'moment';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";


export default class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.state = { areas: [], seletedArea: 3, shops: [], selectedShop: '', 
        selectedShopDetails: {}, handleClose: true, showModal: false, modalMode: "", 
        addBillNumber: '', addBillAmount: '', debitBillAmount: '',selectedShopPlaceholder:'select',
        debitBillDate:new Date() }
        this.setAreas = this.setAreas.bind(this)
        this.getShops = this.getShops.bind(this)
        this.setShops = this.setShops.bind(this)
        this.getShopDetail = this.getShopDetail.bind(this)
        this.shopsDetails = this.shopsDetails.bind(this)
        this.addBill = this.addBill.bind(this)
        this.debitAmount = this.debitAmount.bind(this)
        this.closeModel = this.closeModel.bind(this)
        this.syncToserver = this.syncToserver.bind(this)
        this.billvaluechange = this.billvaluechange.bind(this)
    }
    componentDidMount() {
        firebase.getAreaDetails(this.setAreas)
    }
    setAreas(data) {
        this.setState({ areas: data.area })
    }
    getShops(e) {
        if(e) {
            this.setState({
                seletedArea: e.id,
                selectedShop: null,
                selectedShopPlaceholder:'select'
            }, () => { firebase.getShopDetails(this.setShops, this.state.seletedArea)         
             }) 
        }
    }
    billvaluechange(e,type){
        const re = /^[0-9\b]+$/;
        if(e.target.value === '' || re.test(e.target.value)){
            if(type === 'debit'){
                this.setState({ debitBillAmount: e.target.value })
            } else{
                this.setState({ addBillAmount: e.target.value })                
            }
        }
    }
    getShopDetail(e) {
        if(e){
            this.setState({ selectedShop: e.id,selectedShopPlaceholder:e.shopName }, () => {
                firebase.getCorrespondingShopDetails(this.shopsDetails, this.state.selectedShop)
            })
        }
    }
    setShops(data) {
        this.setState({ shops: data.shops })
    }
    shopsDetails(data) {
        this.setState({ selectedShopDetails: data.hasOwnProperty(`${this.state.selectedShop}`) ? data[`${this.state.selectedShop}`] : {} })
    }
    addBill() {
        let dummyObj = this.state.selectedShopDetails
        if (dummyObj.hasOwnProperty('total')) {
            dummyObj.total = parseInt(dummyObj.total) + parseInt(this.state.addBillAmount)
        } else {
            dummyObj.total = this.state.addBillAmount
        }
        let billobj = {
            billNumber: this.state.addBillNumber,
            billAmount: this.state.addBillAmount,
            addedDate: moment().format("YYYY-MM-DD hh:mm:ss"),
            modifiedDate: moment().format("YYYY-MM-DD hh:mm:ss")
        }
        if (dummyObj.hasOwnProperty('bills')) {
            dummyObj.bills.push(billobj)
        } else {
            dummyObj.bills = []
            dummyObj.bills.push(billobj)
        }
        dummyObj.shopName = this.state.shops.filter((s) => { return s.id === this.state.selectedShop })[0].shopName
        dummyObj.lastModified = moment().format("YYYY-MM-DD hh:mm:ss")
        this.syncToserver(dummyObj)
    }

    debitAmount() {
        let dummyObj = this.state.selectedShopDetails
        let debitObj = {}
        debitObj.amount = this.state.debitBillAmount
        debitObj.debitDate = moment(this.state.debitBillDate).format("YYYY-MM-DD hh:mm:ss")
        if (dummyObj.hasOwnProperty('total')) {
            dummyObj.total = parseInt(dummyObj.total) - parseInt(this.state.debitBillAmount)
        } else {
            alert("There is no due")
        }
        if (!dummyObj.hasOwnProperty('debitDetails')) {
            dummyObj.debitDetails = []
        }
        dummyObj.debitDetails.push(debitObj)
        this.syncToserver(dummyObj)
    }

    syncToserver(dummyObj) {
        this.setState({
            selectedShopDetails: dummyObj,
            debitBillAmount: "",
            addBillAmount: "",
            addBillNumber: ""
        }, async () => {
            let a = await firebase.addDebitBill(this.state.selectedShopDetails, this.state.selectedShop)

        })
    }

    closeModel() {
        this.setState({
            handleClose: true,
            showModal: false
        })
    }

    render() {
        return (
            <div>
                {/* <Form.Row > */}
                <Col lg={2}> <Button
                        className="reportbtn buttons"
                        variant="success"
                        onClick={() => {
                            localStorage.removeItem("bill_uid");
                            this.props.history.push("/");
                        }}
                    ><i className="fas fa-sign-out-alt"></i>
                        Logout
                    </Button></Col>
              
                <Col >

                    <Button
                        className="reportbtn buttons"
                        variant="primary"
                        onClick={() => {
                            this.props.history.push({
                                pathname: '/report',
                                state: this.state.areas
                            })
                        }}
                    >
                        Report
                    </Button>
                    <Button
                        className="reportbtn buttons"
                        variant="danger"
                        onClick={() => {
                            this.props.history.push({
                                pathname: '/admin',
                                state: this.state.areas
                            })
                        }}
                    >
                        Admin
                    </Button>
                </Col>
                {/* </Form.Row> */}
                <Form.Group className="dashboardContainer">
                    <h3 className="dashboardTitle">Select Line</h3>
                    <Form.Row>
                        <Col>
                            <Select
                                className="basic-single"
                                classNamePrefix="select"
                                defaultValue={this.state.seletedArea}
                                isDisabled={false}
                                isLoading={false}
                                isClearable={true}
                                isRtl={false}
                                isSearchable={true}
                                name="linedrpdwn"
                                options={this.state.areas}
                                getOptionLabel={option => `${option.areaName}`}
                                getOptionValue={option => `${option.id}`}
                                onChange={option => this.getShops(option)}
                            />
                            {/* <Form.Control as="select" size="lg" className = "linedrpdwn" defaultValue={this.state.seletedArea} onChange={(e) => {
                                                this.getShops(e);
                    }} >
                        {
                            this.state.areas.map(a => {
                                return (
                                    <option className = "options" key={a.id} value={a.id}
                                    >{a.areaName}</option>
                                )
                            })
                        }
                    </Form.Control> */}
                        </Col>
                    </Form.Row>
                    <Form.Row>
                        <Col >
                            {
                                this.state.shops.length ? (
                                    <div>
                                        <h3 className="dashboardTitle">Select Shop</h3>
                                        <Select
                                            className="linedrpdwn1"
                                            classNamePrefix="select"
                                            defaultValue={''}
                                            isDisabled={false}
                                            isLoading={false}
                                            isClearable={true}
                                            isRtl={false}
                                            isSearchable={true}
                                            name="linedrpdwn1"
                                            placeholder={this.state.selectedShopPlaceholder}
                                            options={this.state.shops}
                                            getOptionLabel={option => `${option.shopName}`}
                                            getOptionValue={option => `${option.id}`}
                                            onChange={option => this.getShopDetail(option)}
                                        />
                                        {/* <Form.Control as="select" className = "linedrpdwn" size="lg" defaultValue="0" onChange={(e) => {
                                this.getShopDetail(e);
                            }}>
                                {
                                    this.state.shops.map(a => {
                                        return (
                                            <option className = "options" key={a.id} value={a.id}
                                            >{a.shopName}</option>
                                        )
                                    })
                                }
                            </Form.Control> */}
                                    </div>

                                ) : ('')
                            }
                        </Col>
                    </Form.Row>

                </Form.Group>
                {this.state.selectedShop ? (

                    <div>
                        <h1 className="shpname">{this.state.shops.filter((s) => { return s.id === this.state.selectedShop })[0].shopName}</h1>
                        {
                            this.state.selectedShopDetails.total ? (
                                <h3 className="dueamnt">Due Amount : <span className="amnt">{this.state.selectedShopDetails.total}</span></h3>
                            ) : (<h3 className="dueamnt">No Bills found</h3>)
                        }
                        <div className="buttongroup">
                            <Button
                                className="buttons"
                                variant="success"
                                onClick={() => {
                                    this.setState({
                                        showModal: true,
                                        modalMode: 'ADD',
                                        addBillNumber: '',
                                        addBillAmount: ''
                                    })
                                }}
                            >
                                ADD BILL
                             </Button>
                           { this.state.selectedShopDetails.total > 0 ? ( <Button
                                className="buttons"
                                variant="danger"
                                onClick={() => {
                                    this.setState({
                                        showModal: true,
                                        modalMode: 'DEBIT',
                                        addBillNumber: '',
                                        addBillAmount: ''
                                    })
                                }}
                            >
                                DEBIT BILL
                            </Button>) : ('')}
                           
                        </div>
                    </div>
                ) : ('')}
                <Modal show={this.state.showModal} onHide={this.closeModel}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.state.modalMode} Bill </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {this.state.modalMode === "ADD" ? (
                            <Form.Group>
                                <Form.Control type="text" placeholder="Bill Number" value={this.state.addBillNumber} onChange={(e) => { this.setState({ addBillNumber: e.target.value }) }} /><br />
                                <Form.Control type="text" placeholder="Bill Amount" value={this.state.addBillAmount} onChange={(e) => { this.billvaluechange(e,'add') }} />
                            </Form.Group>
                        ) : (<Form.Group>
                            <Form.Control type="text" placeholder="Bill Amount" value={this.state.debitBillAmount} onChange={(e) => { this.billvaluechange(e,'debit') }} />
                            <br/><label className="labels">Choose Date</label>
                            <DatePicker
                            placeholderText="Choose Date"
                            selected={this.state.debitBillDate}
                            onChange={(date) => {
                                this.setState({
                                    debitBillDate: date
                                })
                            }}
                            maxDate={new Date()}
                            showDisabledMonthNavigation />
                        </Form.Group>)}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.closeModel}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={() => {
                            if (this.state.modalMode === "ADD") {
                                this.addBill()
                            }
                            else {
                                this.debitAmount()
                            }
                            this.setState({
                                handleClose: true,
                                showModal: false
                            })
                        }}>
                            Save Changes
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
