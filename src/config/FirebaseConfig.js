

import app from "firebase/app";
import "firebase/auth";
import "firebase/firebase-firestore";


const firebaseConfig = {
    apiKey: "AIzaSyCqT2_esuNLrVZP1z9gnAFp1gS0loPcZ6g",
    authDomain: "bill-709ba.firebaseapp.com",
    projectId: "bill-709ba",
    storageBucket: "bill-709ba.appspot.com",
    messagingSenderId: "957700882002",
    appId: "1:957700882002:web:b543c0c058b76f569fe98b",
    measurementId: "G-J6BLF8R0VL"
};

class Firebase {
    constructor() {
        app.initializeApp(firebaseConfig);
        this.auth = app.auth();
        this.db = app.firestore();
        this.selectedPost = {};
    }

    login(email, password) {
        return this.auth.signInWithEmailAndPassword(email, password);
    }

    logout() {
        return this.auth.signOut();
    }

    async register(name, email, password) {
        await this.auth.createUserWithEmailAndPassword(email, password);
        return this.auth.currentUser.updateProfile({
            displayName: name
        });
    }

    addUser(userobj) {
        if (!this.auth.currentUser) {
            return alert("Not authorized");
        }
        return this.db.doc(`/users/${this.auth.currentUser.uid}`).set({
            [this.auth.currentUser.uid]: userobj
        });
    }
    addArea(areaArr,newId) {
        if (!this.auth.currentUser) {
            return alert("Not authorized");
        }
        this.db.doc('/area/5bMAc7668xI9AzjaPHP0').set({
            area: areaArr
        })
        return this.db.doc(`/area/${newId}`).set({
            'shops': []
        })
    }
    addShopToArea(data, shopid){
        if (!this.auth.currentUser) {
            return alert("Not authorized");
        }
        this.db.doc(`/shop/${shopid}`).set({
            [shopid]: data
        })
    }
    addDebitBill(billObj,shopId) {
        return this.db.doc(`/shop/${shopId}`).set({
            [shopId]: billObj
        });
    }
    getUserDetails(cb) {
        return this.db
            .collection("users")
            .doc(`${this.auth.currentUser.uid}`)
            .get()
            .then((s) => {
                if (s.exists) {
                    cb(s.data());
                }
            });
    }
    getAreaDetails(cb) {
        this.db
            .collection("area")
            .doc('5bMAc7668xI9AzjaPHP0')
            .get()
            .then((s) => {
                if (s.exists) {
                    cb(s.data())
                }
            });
    }
    getShopDetails(cb,areaId) {
        this.db
            .collection("area")
            .doc(`${areaId.toString()}`)
            .get()
            .then((s) => {
                if (s.exists) {
                    cb(s.data())
                }
            });
    }
    putShopDetails(areaId,data) {
        this.db
            .doc(`/area/${areaId}`)
            .set({
                shops:data 
            })
    }
    getAllShops(cb) {
              this.db
            .collection("shop")            
            .get()
            .then((s) => {
                if (s.docs.length) {
                    cb(s.docs.map(doc => doc.data()))
                }
            });
    }
    getCorrespondingShopDetails(cb, shopId){
        this.db
        .collection("shop")
        .doc(`${shopId.toString()}`)
        .get()
        .then((s) => {
            if (s.exists) {
                cb(s.data())
            } else{
                cb({})
            }
        });
    }
    isInitialized() {
        return new Promise((resolve) => {
            this.auth.onAuthStateChanged(resolve);
        });
    }
    getCurrentUsername() {
        return this.auth.currentUser && this.auth.currentUser.displayName;
    }
}
export default new Firebase();